/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3A;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;

/**
 *
 * @author asus
 */
public class Latihan3a extends JFrame {

    public Latihan3a() {
        this.setSize(300, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini Class turunan dari class JFrame");
        this.setVisible(true);

        JPanel panel = new JPanel();
        JButton tombol = new JButton();
        tombol.setText("Ini Tombol");
        panel.add(tombol);
        this.add(panel);
        
    }

    public static void main(String[] args) {
        Latihan3a latihan3a = new Latihan3a();
    }
}
