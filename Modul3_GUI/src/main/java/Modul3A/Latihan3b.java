/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3A;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class Latihan3b extends JFrame{
    public Latihan3b (){
        this.setSize(300, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini class turunan dari class JFrame");
        this.setVisible(true);
        
        JPanel panel =new JPanel(null);
        
        
        JButton tombol = new JButton("Ini Tombol");
        tombol.setBounds(150, 20, 110, 20);
        tombol.add(tombol);
        
        JLabel label = new JLabel("Ini Label");
        label.setBounds(150, 50, 150, 20);
        label.add(label);
        
        JTextField text = new JTextField("Ini Text");
        text.setBounds(150, 80, 190, 20);
        text.add(text);
        
        JCheckBox box = new  JCheckBox("Ini Box");
        box.setBounds(150, 110, 230, 20);
        box.add(box);
        
        JRadioButton radio = new JRadioButton("Ini Radio");
        radio.setBounds(150, 140, 260, 20);
        radio.add(radio);
        
    }
    public static void main(String[] args) {
        Latihan3b latihan3b = new Latihan3b();
    }
}
