/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3B;
import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 *
 * @author asus
 */
public class Latihan1c extends JFrame {

    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 250;
    private static final int FRAME_X_ORIGIN = 300;
    private static final int FRAME_Y_ORIGIN = 100;
    private static final int BUTTON_WIDGHT = 80;
    private static final int BUTTON_HEIGHT = 30;
    private JButton cancelButton;
    private JButton okButton;
    private JTextField txtField;
    
    public static void main(String[] args) {
        Latihan1c frame = new Latihan1c();
        frame.setVisible(true);
    }
    public Latihan1c(){
        Container contentPane  =  getContentPane();
        setSize(FRAME_WIDTH,FRAME_HEIGHT);
        setResizable(true);
        setTitle("Program Latihan1");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
        contentPane.setLayout(null);
        contentPane.setBackground(Color.white);
        JPanel panel = new JPanel();
        
        JLabel label1 = new JLabel();
        JLabel label2 = new JLabel();
        JLabel label3 = new JLabel();
        label1.setText("Nama            :");
        label1.setBounds(30, 20, 110, 20);
        contentPane.add(label1);
        label2.setText("Jenis Kelamin   :");
        label2.setBounds(30, 40, 130, 20);
        contentPane.add(label2);
        label3.setText("Hobi            :");
        label3.setBounds(30, 60, 150, 20);
        contentPane.add(label3);
        
        JTextField text = new JFormattedTextField();
        text.setBounds(150, 20, 130, 20);
        contentPane.add(text);
        
        JRadioButton radio1 = new JRadioButton();
        radio1.setText("Laki-laki");
        radio1.setBounds(150, 40, 130, 20);
        contentPane.add(radio1);
        JRadioButton radio2 = new JRadioButton();
        radio2.setText("Perempuan");
        radio2.setBounds(300, 40, 130, 20);
        contentPane.add(radio2);
        
        JCheckBox box1 = new JCheckBox();
        box1.setText("Olaraga");
        box1.setBounds(150, 60, 130, 20);
        this.add(box1);
        JCheckBox box2 = new JCheckBox();
        box2.setText("Shooping");
        box2.setBounds(150, 80, 130, 20);
        contentPane.add(box2);
        JCheckBox box3 = new JCheckBox();
        box3.setText("Computer Games");
        box3.setBounds(150, 100, 130, 20);
        contentPane.add(box3);
        JCheckBox box4 = new JCheckBox();
        box4.setText("Nonton Bioskop");
        box4.setBounds(150, 120, 130, 20);
        contentPane.add(box4);
        
        okButton = new JButton("Ok");
        okButton.setBounds(150, 150, BUTTON_WIDGHT, BUTTON_HEIGHT);
        contentPane.add(okButton);
        
        
        cancelButton  = new JButton("Cancel");
        cancelButton.setBounds(300, 150, BUTTON_WIDGHT, BUTTON_HEIGHT);
        contentPane.add(cancelButton);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
